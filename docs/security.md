# Network Security and Securing Devices

## Usernames and passwords

### Set username and password

```cisco
switch> enable
switch# configure terminal
switch(config)# username <username> secret <password>
```

### Set password on enable mode

```cisco
switch> enable
switch# configure terminal
switch(config)# enable secret <password>
```

### Scramble plaintext password in config

```cisco
switch> enable
switch# configure terminal
switch(config)# service password-encryption
```

## Activate SSH

### Generate RSA keys

```cisco
switch> enable
switch# configure terminal
Switch(config)# ip domain-name <domain-name>
Switch(config)# crypto key generate rsa
```

### Set password on SSH

```cisco
switch> enable
switch# configure terminal
switch(config)# line vty 0 - 4
switch(config-line)# password <password>
switch(config-line)# login local
```

## Access lists (ACL)

### New access list

```cisco
Router> enable
Router# configure terminal

// The access lists from 1-99 are basic lists, and it is recommended to use the extended access lists (100 and above) to have more options rules

Router(config)# access-list <100 - > [permit, deny, remark] <proto> host <source> host <dest> [eq, range] <port> <port>
```

### Show current access lists

```cisco
Router> enable
Router# show access-list
```

### Delete ACL group

```cisco
Router> enable
Router# configure terminal
Router(config)# ip access-list [standard, extended] <ACL group id>
Router(config-ext-nacl)# no <ACL group id>
```

### Add new rule to specific ACL id

```cisco
Router> enable
Router# configure terminal
Router(config)# ip access-list <standard, extended> <ACL group id>
Router(config-ext-nacl)# <Line id> [permit, deny, remark] <proto> host <source> host <dest> [eq, range] <port> <port>
```

### Enable ACL on interface

```cisco
Router> enable
Router# configure terminal
Router(config)# interface fastethernet0/0
Router(config-if)# ip access-group <ACL groupe ID>
```

## AAA

* Nothing here yet...
(Want to contribute? Make a pull request to the
[Cisco-cheat-sheet](https://gitlab.com/nerds-in-the-corner/Cisco-cheat-sheet)

## LAN-to-LAN IPsec Tunnel (pre-shared key)

```cisco
Router A:

Router> enable
Router# configure terminal

// Configure Isakmp
Router(config)# crypto isakmp policy <Number>
Router(config-isakmp)# hash <md5/sha>
Router(config-isakmp)# authentication pre-share
Router(config-isakmp)# exit

// Configure Crypto Map
Router(config)# crypto isakmp key vpnuser address <address>
Router(config)# crypto ipsec transform-set <WORD> <choise of transform> <choise of transform>
Router(config)# crypto map <word> <Number> ipsec-isakmp
Router(config-crypto-map)# set peer <IP for WAN acces router site 2>
Router(config-crypto-map)# set transform-set <WORD>
Router(config-crypto-map)# match address <NUMBER/WORD>
Router(config-crypto-map)# exit

Router(config)# interface <interface>
Router(config-if)# crypto map <WORD>
Router(config-if)# exit
Router(config)# access-list <NUMBER/WORD> permit <LAN IP site 1 wildcard> <LAN IP site 2 wildcard>

// For router B swap the sites and repeat
```
