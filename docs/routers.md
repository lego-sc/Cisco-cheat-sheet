# Routers

## Configuring ip addresses on interfaces

### Configuring an IPv4 address on an interface

```cisco
Router> enable
Router# configure terminal
Router(config)# interface fastethernet0/0
Router(config-if)# no shutdown
Router(config-if)# ip address <gateway IP> <subnet>
```

### Configuring an IPv6 address on an interface

```cisco
Router> enable
Router# configure terminal
Router(config)# interface fastethernet0/0
Router(config-if)# no shutdown
Router(config-if)# ipv6 address <IPv6> <link-local>
```

## Tag vLANs

### Make a sub-interface on an interface

```cisco
Router> enable
Router# configure terminal
Router(config)# interface fastethernet0/0.<subinterface number>
```

### Enabling dot1Q encapsulation

```cisco
Router> enable
Router# configure terminal
Router(config)# interface fastethernet0/0.<subint number>
Router(config-subif)# encapsulation dot1Q <vlan number>
```

## Dynamic Host Configuration Protocol (DHCP)

### Using an external DHCP server

```cisco
Router> enable
Router# configure terminal
Router(config)# interface fastethernet0/0
Router(config-if)# ip helper-address <DHCP server IP>
```

### IPv4 DHCP server

* Nothing here yet...
(Want to contribute? Make a pull request to the
[Cisco-cheat-sheet](https://gitlab.com/nerds-in-the-corner/Cisco-cheat-sheet))

### IPv6 DHCP

#### Stateless Address Autoconfiguration (SLAAC)

##### Configuring a DHCP pool

```cisco
Router> enable
Router# configure terminal
Router(config)# ipv6 dhcp pool <dhcp pool name>
Router(config-dhcpv6)# dns-server <IPv6 DNS>
```

##### Configuring an interface that DHCP should advertise on

```cisco
Router> enable
Router# configure terminal
Router(config)# interface g0/0
Router(config-if)# ipv6 dhcp server <dhcp pool name>
Router(config-if)# ipv6 nd other-config-flag
```

*You need to [Activate unicast routing](#activate-unicast-routing) to let the client make its own IPv6 address*

## Network Adress Translation (NAT)

### Static NAT

#### Setting static NAT with ip address

```cisco
router> enable
router# configure terminal
router(config)# ip nat [inside, outside] source static <inside local IP A.B.C.D> <inside global IP A.B.C.D>
```

#### Configuring static NAT with port number

```cisco
router> enable
router# configure terminal
router(config)# ip nat [inside, outside] source static [tcp, udp] <local ip A.B.C.D> <port number> <inside global ip A.B.C.D> <port number>
```

#### define inside and outside interface

```cisco
router> enable
router# configure terminal
router(config)# interface <interface>
router(config-if)# ip nat <inside, outside>
```

## IPv6

### Activate unicast routing

```cisco
router> enable
router# configure terminal
router(config)# ipv6 unicast-routing
```

### IPv6 address

```cisco
router> enable
router# configure terminal
router(config)# interfaces fastethernet<>/<>
router(config)# ipv6 address <ipv6 address>
```

#### Show Interface IPv6 address

```cisco
router> enable
router# show ipv6 interface brief
```
