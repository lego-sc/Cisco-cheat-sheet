# Configure Cisco Network

## Cheat sheet

Made with <3 by Marius Solberg & Kjetil Storesund & Eivind Nygård

Source: [https://gitlab.com/nerds-in-the-corner/Cisco-cheat-sheet](https://gitlab.com/nerds-in-the-corner/Cisco-cheat-sheet)

---

* Want to contribute? Make a pull request to the
[Cisco-cheat-sheet](https://gitlab.com/nerds-in-the-corner/Cisco-cheat-sheet)

---
