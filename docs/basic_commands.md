# Basic Commands

## Write current running-config to startup-config

```cisco
switch> enable
switch# copy running-config startup-config
```

## Alternative to writing current running-config to startup-config

```cisco
switch> enable
switch# write memory
```

## Erase current config

```cisco
switch> enable
switch# write erase
```

After `write erase` reload the server

## Reboot devices

```cisco
switch> enable
switch# reload
```

## Show config

### Running config

```cisco
switch> enable
switch# show running-config
```

### Startup config

```cisco
switch> enable
switch# show startup-config
```

## Reset interface to default settings

```cisco
switch> enable
switch(config)# default interface [vlan, fastethernet] <Interface number>
```

## Select multiple interfaces

```cisco
switch> enable
switch# configure terminal
switch(config)# interface range <interface<x-y>>
```

## Set hostname on device

```cisco
switch> enable
switch# configure terminal
switch(config)# hostname <hostname>
```

## Disable domain lookup

```cisco
switch> enable
switch# configure terminal
switch(config)# no ip domain-lookup
```
