# Routing Protocols

## OSPF

### Setting up OSPF with IPv4

```cisco
Router> enable
Router# configure terminal
Router(config)# router ospf <process id>
Router(config)# network <neighboring networks> <Wildcard> area <area id>
//enable the routing on an interface
Router(config)# interface <interface>
Router(config-if)# ip ospf <process id> area <area id>
```

### Setting up OSPF with IPv6

```cisco
Router> enable
Router# configure terminal
Router(config)# ipv6 router ospf <Process id>
Router(config-rtr)# router-id <router-id>
Router(config-rtr)# exit
Router(config)# interface fastethernet0/0
Router(config-if)# ipv6 ospf <Process ID> area <area id>
```

#### Show OSPF routing table

##### Show IPv4 routing table

```cisco
Router> enable
Router# show ip route ospf
```

##### Show IPv6 routing table

```cisco
Router> enable
Router# show ipv6 route ospf
```

## Hot Standby Router Protocol (HSRP)

* Hot Standby Router Protocol is used on networks where two or more routers are used. Router A is active and Router B is on standby and assumes routing responibilities if Router A fails or loses connection.

### Requirements to set up hot standby routing protocol

* Router A: An interface (g0/0/0) is facing the wan/web and one interface (g0/0/1) faces the LAN (switch1)
* Router B: An interface (g0/0/0) is towards the wan/web and one interface (g0/0/1) towards LAN (switch2)
* switch1 and switch2 is directly connected

### Not a requirement but useful

* Setting up a routing protocol on router A and router B

---

#### Configuring Router A as the active gateway

```cisco
RouterA> enable
RouterA# configure terminal
RouterA(config)# interface gigabitEthernet 0/0/1
RouterA(config-if)# ip address <local ip x.x.x.2>
RouterA(config-if)# standby 1 ip <local ip x.x.x.1>

//preempt makes the connection with highest priority to assume routing priviliges
RouterA(config-if)# standby 1 preempt

//default priority is 100, choose a priority above 100 for the main router
RouterA(config-if)# standby 1 priority 105

//track reduces priority by 10 if connection is lost
RouterA(config-if)# standby 1 track gigabitEthernet 0/0/0
```

#### Configuring Router B as the passive gateway

```cisco
RouterB> enable
RouterB# configure terminal
RouterB(config)# interface gigabitEthernet 0/0/1
RouterB(config-if)# ip address <local ip x.x.x.3>
RouterB(config-if)# standby 1 ip <local ip x.x.x.1>
RouterB(config-if)# standby 1 preempt
```

#### Setting up the clients

* Set default gateway to the standby 1 ip address ([local ip x.x.x.1])

### Hot standby routing protocol status

```cisco
Router> enable
Router# show standby
```
