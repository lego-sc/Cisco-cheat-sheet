# Switches

## vLANs

### Make vLAN

```cisco
switch> enable
switch# configure terminal
switch(config)# vlan <vlan id>
Switch(config-vlan)# name <vlan name>
```

### Set vlan on interface

```cisco
switch> enable
switch# configure terminal
switch(config)# interface <interface>
switch(config-if)# switchport mode access
switch(config-if)# switchport access vlan<vlan id>
```

### Trunk vLANs

```cisco
switch> enable
switch# configure terminal
switch(config)# interface <interface>
switch(config-if)# switchport mode trunk
switch(config-if)# switchport trunk allowed vlan [add, all, remove, none] <vlan id>
```

### Show all vLANs

```cisco
switch> enable
switch# show vlans
```

## Link aggregation

```cisco
switch> enable
switch# configure terminal
switch(config)# interface <interface>
switch(config-if)# channel-group <NUMBER> mode <MODE>
switch(config-if)# exit

// After all desireable ports are chosen for a link, configure the link as a single link done above through the channel group.
Switch(config)# interface port-channel <NUMBER>
Switch(config-if)#
```
